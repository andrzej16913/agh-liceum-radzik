import math

def function(number):
	max_number = int(math.sqrt(number))
	for i in range(2, max_number):
		if number % i == 0:
			return False
	return True

number = int(input())
print(function(number))